package com.example.roadster_companion;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.SystemClock;
import androidx.core.content.ContextCompat;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;
import java.util.StringTokenizer;
import android.location.Location;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static Button send_destination;
    public static Button start;
    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private static final String TAG = MapsActivity.class.getSimpleName();
    SupportMapFragment mapFragment;
    private TextView mBluetoothStatus;
    private static final int DEFAULT_ZOOM = 15;
    private final LatLng defaultLocation = new LatLng(-33.8523341, 151.2106085);
    private BluetoothAdapter mBTAdapter;
    private Set<BluetoothDevice> mPairedDevices;
    private ArrayAdapter<String> mBTArrayAdapter;
    private ListView mDevicesListView;
    Button connect_Btn;
    Button stop;
    Button send_cpts;
    Button clear;
    Vector<LatLng> checkpoints=new Vector<LatLng>(1);
    TextView speed;
    TextView distance;
    TextView left;
    TextView right;
    TextView center;
    TextView back;
    TextView compass;
    TextView pwm;
    TextView rps;
    TextView waypoint;
    TextView compass_raw;
    TextView battery;
    TextView sending_status;
    ProgressBar progress;
    TextView car_status;

    private Handler mHandler; // Our main handler that will receive callback notifications
    private ConnectedThread mConnectedThread; // bluetooth background worker thread to send and receive data
    private BluetoothSocket mBTSocket = null; // bi-directional client-to-client data path

    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); // "random" unique identifier
    private boolean locationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location lastKnownLocation;

    // #defines for identifying shared types between calling functions
    private final static int REQUEST_ENABLE_BT = 1; // used to identify adding bluetooth names
    private final static int MESSAGE_READ = 2; // used in bluetooth handler to identify message update
    private final static int CONNECTING_STATUS = 3; // used in bluetooth handler to identify message status

    boolean init=true;
    boolean state=false;
    boolean move_camera=false;
    Integer compass_value=0;
    Marker prev=null;
    String destination_coordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        //send_destination=findViewById(R.id.send_destination);
        start=findViewById(R.id.start);
        connect_Btn = (Button)findViewById(R.id.button);
        stop=(Button)findViewById(R.id.stop);
        clear=(Button)findViewById(R.id.clear);
        send_cpts=(Button)findViewById(R.id.send_checkpoints);
        left=(TextView)findViewById(R.id.left);
        right=(TextView)findViewById(R.id.right);
        center=(TextView)findViewById(R.id.center);
        speed=(TextView)findViewById(R.id.speed);
        distance=(TextView)findViewById(R.id.distance);
        back=(TextView)findViewById(R.id.back);
        compass=(TextView)findViewById(R.id.comp);
        waypoint=(TextView)findViewById(R.id.waypoint);
        pwm=(TextView)findViewById(R.id.pwm);
        rps=(TextView)findViewById(R.id.rps);
        compass_raw=(TextView)findViewById(R.id.textView16);
        battery=(TextView)findViewById(R.id.battery);
        sending_status=(TextView)findViewById(R.id.textView6);
        car_status=(TextView)findViewById(R.id.textView3);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //progress=(ProgressBar) findViewById(R.id.progressBar5);
        //progress.setMax(200);

        mBTArrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        mBTAdapter = BluetoothAdapter.getDefaultAdapter(); // get a handle on the bluetooth radio
        mBluetoothStatus = (TextView)findViewById(R.id.bluetooth_status);
        mDevicesListView = (ListView)findViewById(R.id.devicesListView);
        mDevicesListView.setAdapter(mBTArrayAdapter); // assign model to view
        mDevicesListView.setOnItemClickListener(mDeviceClickListener);
       // speed=(TextView)findViewById(R.id.speed_view);
        destination_coordinates="GPS," + 0 + "," + 0 +"\n";
        mHandler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage (@NonNull  android.os.Message msg){
                String readMessage ="";
                if(msg.what == MESSAGE_READ){
                    StringTokenizer message=null;
                    String received_line =null;
                    String read;
                    readMessage = readMessage+(String)msg.obj;

                    if(readMessage.indexOf("\n")>0) {
                        message = new StringTokenizer(readMessage, "\n");
                        StringTokenizer st;
                        while (message.hasMoreTokens()) {
                            st = null;
                            received_line = message.nextToken();
                            st = new StringTokenizer(received_line, ",");
                            try {
                                read = st.nextToken();
                            } catch (Exception e) {
                                continue;
                            }
                            if (read.compareTo("GPS") == 0) {
                                try {
                                    LatLng current_location = new LatLng(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()));
                                    waypoint.setText(st.nextToken("\n").replace(",", ""));
                                    prev.remove();

                                    prev = mMap.addMarker(new MarkerOptions().position(current_location).anchor(0.5f,0.5f).rotation(compass_value).title("Roadster").icon(BitmapFromVector(getApplicationContext(), R.drawable.ic_baseline_directions_car_filled_24)));

                                    if (state || init) {
                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(current_location));
                                        if (current_location.latitude != 0) init = false;
                                    }
                                } catch (Exception e) {
                                }
                            } else if (read.compareTo("speed") == 0) {
                                try {
                                    speed.setText(st.nextToken("\n").replace(",", "") + "m/s");
                                } catch (Exception e) {
                                }
                            } else if (read.compareTo("sens") == 0) {
                                try {
                                    left.setText(st.nextToken() + "cm");
                                    right.setText(st.nextToken() + "cm");
                                    center.setText(st.nextToken() + "cm");
                                    back.setText(st.nextToken("\n").replace(",", "") + "cm");
                                } catch (Exception e) {
                                }
                            } else if (read.compareTo("comp") == 0) {
                                try {
                                    compass.setText(st.nextToken());
                                    String compass_s=st.nextToken("\n").replace(",", "");
                                    compass_raw.setText(compass_s);
                                    compass_value =Integer.parseInt(compass_s);
                                    prev.setAnchor(0.5f,0.5f);
                                    prev.setRotation(compass_value);

                                } catch (Exception e) {
                                }
                            } else if (read.compareTo("dist") == 0) {
                                try {
                                    String dis=st.nextToken("\n").replace(",", "");
                                    distance.setText(dis+"m");
                                    //int prog=(int)Float.parseFloat(dis)%200;
                                    //progress.setProgress(prog);
                                } catch (Exception e) {
                                }
                            } else if (read.compareTo("mot") == 0) {
                                try {
                                    rps.setText(st.nextToken());
                                    pwm.setText(st.nextToken("\n").replace(",", ""));
                                } catch (Exception e) {
                                }

                            }
                            else if(read.compareTo("bat")==0){
                                try{
                                    battery.setText(st.nextToken("\n").replace(",", "")+"%");
                                }catch (Exception e){

                                }
                            }

                        }
                        readMessage="";
                    }
                }

                if(msg.what == CONNECTING_STATUS){
                    if(msg.arg1 == 1)
                        mBluetoothStatus.setText("Connected to Device: " + (String)(msg.obj));
                    else
                        mBluetoothStatus.setText("Connection Failed");
                }
            }
        };
        connect_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listPairedDevices(v);
            }
        });
        /**
        send_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    try {

                        mConnectedThread.write(destination_coordinates);
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(),"Connect to Roadster First",Toast.LENGTH_SHORT).show();
                    }

            }
        });**/

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if(!state) {
                        mConnectedThread.write("--,\n");
                        mMap.clear();
                        checkpoints.clear();
                        sending_status.setText("Waiting");
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Stop the car First",Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Connect to Roadster First",Toast.LENGTH_SHORT).show();
                }

            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mConnectedThread.write("!!,\n");
                    state=false;
                    move_camera=false;
                    car_status.setText("   stopped");
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Connect to Roadster First",Toast.LENGTH_SHORT).show();
                }

            }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    mConnectedThread.write("##,\n");
                    state=true;
                    move_camera=true;
                    car_status.setText("   started");
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Connect to Roadster First",Toast.LENGTH_SHORT).show();
                }

            }
        });
        send_cpts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    sending_status.setText("....");
                for(int i=0;i<checkpoints.size();i++) {
                    String cpt="GPS,"+checkpoints.get(i).latitude+","+checkpoints.get(i).longitude+"\n";
                    mConnectedThread.write(cpt);
                }
                sending_status.setText("Sent");
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Connect to Roadster First",Toast.LENGTH_SHORT).show();
                }

            }
        });



    }

    private void bluetoothOn(View view){
        if (!mBTAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            mBluetoothStatus.setText("Bluetooth enabled");
            Toast.makeText(getApplicationContext(),"Bluetooth turned on",Toast.LENGTH_SHORT).show();

        }
        else{
            Toast.makeText(getApplicationContext(),"Bluetooth is already on", Toast.LENGTH_SHORT).show();
        }
    }

    // Enter here after user selects "yes" or "no" to enabling radio
/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent Data){
        // Check which request we're responding to
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.
                mBluetoothStatus.setText("Enabled");
            }
            else
                mBluetoothStatus.setText("Disabled");
        }
    }*/

    private void bluetoothOff(View view){
        mBTAdapter.disable(); // turn off
        mBluetoothStatus.setText("Bluetooth disabled");
        Toast.makeText(getApplicationContext(),"Bluetooth turned Off", Toast.LENGTH_SHORT).show();
    }

    private void discover(View view){
        // Check if the device is already discovering
        if(mBTAdapter.isDiscovering()){
            mBTAdapter.cancelDiscovery();
            Toast.makeText(getApplicationContext(),"Discovery stopped",Toast.LENGTH_SHORT).show();
        }
        else{
            if(mBTAdapter.isEnabled()) {
                mBTArrayAdapter.clear(); // clear items
                mBTAdapter.startDiscovery();
                Toast.makeText(getApplicationContext(), "Discovery started", Toast.LENGTH_SHORT).show();
                registerReceiver(blReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
            }
            else{
                Toast.makeText(getApplicationContext(), "Bluetooth not on", Toast.LENGTH_SHORT).show();
            }
        }
    }

    final BroadcastReceiver blReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // add the name to the list
                mBTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                mBTArrayAdapter.notifyDataSetChanged();
            }
        }
    };

    private void listPairedDevices(View view){
        mPairedDevices = mBTAdapter.getBondedDevices();
        if(mBTAdapter.isEnabled()) {
            // put it's one to the adapter
            for (BluetoothDevice device : mPairedDevices)
                mBTArrayAdapter.add(device.getName() + "\n" + device.getAddress());

            Toast.makeText(getApplicationContext(), "Show Paired Devices", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(getApplicationContext(), "Bluetooth not on", Toast.LENGTH_SHORT).show();
    }

    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            if(!mBTAdapter.isEnabled()) {
                Toast.makeText(getBaseContext(), "Bluetooth not on", Toast.LENGTH_SHORT).show();
                return;
            }

            mBluetoothStatus.setText("Connecting...");
            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            final String address = info.substring(info.length() - 17);
            final String name = info.substring(0,info.length() - 17);

            // Spawn a new thread to avoid blocking the GUI one
            new Thread()
            {
                public void run() {
                    boolean fail = false;

                    BluetoothDevice device = mBTAdapter.getRemoteDevice(address);

                    try {

                        mBTSocket = createBluetoothSocket(device);
                    } catch (IOException e) {
                        fail = true;
                        Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_SHORT).show();
                    }
                    // Establish the Bluetooth socket connection.
                    try {
                        mBTSocket.connect();

                    } catch (IOException e) {
                        try {
                            fail = true;
                            mBTSocket.close();
                            mHandler.obtainMessage(CONNECTING_STATUS, -1, -1)
                                    .sendToTarget();
                        } catch (IOException e2) {
                            //insert code to deal with this
                            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if(fail == false) {
                        mConnectedThread = new ConnectedThread(mBTSocket);
                        mConnectedThread.start();

                        mHandler.obtainMessage(CONNECTING_STATUS, 1, -1, name)
                                .sendToTarget();
                    }
                }
            }.start();
        }
    };

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connection with BT device using UUID
    }
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final java.io.InputStream mmInStream;
        private final java.io.OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            java.io.InputStream tmpIn = null;
            java.io.OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[2048];  // buffer store for the stream
            int bytes; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.available();
                    if(bytes != 0) {
                        SystemClock.sleep(50); //pause and wait for rest of data. Adjust this depending on your sending speed.
                        bytes = mmInStream.available(); // how many bytes are ready to be read?
                        bytes = mmInStream.read(buffer, 0, bytes); // record how many bytes we actually read
                        String readMessage = new String(buffer, 0, bytes);
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, readMessage)
                                .sendToTarget(); // Send the obtained bytes to the UI activity

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String input) {
            byte[] bytes = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sjsu = new LatLng(37.337235, -121.882670);
        //mMap.addMarker(new MarkerOptions().position(sjsu).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sjsu,15));
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            locationPermissionGranted=true;
            getDeviceLocation();

        }else{
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
            getDeviceLocation();
        }
        mMap.setMapType(mMap.MAP_TYPE_SATELLITE);
        prev=mMap.addMarker(new MarkerOptions().position(new LatLng(0,0)).title("Roadster"));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                int precision = (int) Math.pow(10,6);

                double new_latitude = (double)((int)(precision*latLng.latitude))/precision;
                double new_longitude = (double)((int)(precision*latLng.longitude))/precision;

                LatLng myloc = new LatLng(latLng.latitude, latLng.longitude);
                mMap.addMarker(new MarkerOptions().position(myloc).title("Destination"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myloc,20));
                checkpoints.add(new LatLng(new_latitude,new_longitude));
                destination_coordinates = "GPS," + new_latitude + "," + new_longitude +"\n";

            }
        });

    }
    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);

        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        // below line is use to draw our
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);

        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        switch (requestCode) {
            case 44: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted=true;
                    //getDeviceLocation();
                }
            }
        }
//        Toast.makeText(MapsActivity.this, "im here 2", Toast.LENGTH_SHORT).show();
//        updateLocationUI();
    }
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */

        try {
            if(locationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            lastKnownLocation = task.getResult();
                            if (lastKnownLocation != null) {
                                mMap.addMarker(new MarkerOptions().position(new LatLng(lastKnownLocation.getLatitude(),
                                        lastKnownLocation.getLongitude())).title("Roadster"));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(lastKnownLocation.getLatitude(),
                                                lastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.addMarker(new MarkerOptions().position(defaultLocation).title("Marker"));
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(defaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (Exception e)  {
            Log.e("Exception: %s", e.getMessage(), e);


        }
    }


}